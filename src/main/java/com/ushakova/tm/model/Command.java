package com.ushakova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class Command {

    @Nullable
    public String arg = "";

    @Nullable
    public String name = "";

    @Nullable
    public String description = "";

    public Command(@NotNull String name, @Nullable String arg, @Nullable String description) {
        this.arg = arg;
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        String result = "";
        result = name != null && !name.isEmpty() ? result + name : result;
        result = arg != null && !arg.isEmpty() ? result + " [" + arg + "] " : result;
        result = description != null && !description.isEmpty() ? result + " - " + description : result;
        return result;
    }

}
