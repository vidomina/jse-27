package com.ushakova.tm.api.service;

import com.ushakova.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull
    Project add(@Nullable String name, @Nullable String description, @Nullable String userId);

}
