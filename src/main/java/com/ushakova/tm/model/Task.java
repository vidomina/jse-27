package com.ushakova.tm.model;

import com.ushakova.tm.api.entity.IWBS;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractBusinessEntity implements IWBS {

    @Nullable
    private String projectId;

    public Task(@NotNull String name) {
        this.name = name;
    }

    @Override
    @NotNull
    public String toString() {
        return "Id: " + this.getId() + "\nTitle: " + name
                + "\nDescription: " + description
                + "\nStatus: " + status.getDisplayName()
                + "\nStart Date:" + dateStart
                + "\nCreated: " + dateCreate;
    }

}
