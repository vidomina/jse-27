package com.ushakova.tm.model;

import com.ushakova.tm.enumerated.Status;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AbstractBusinessEntity extends AbstractEntity {

    @NotNull
    protected String name = "";

    @Nullable
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date dateStart;

    @Nullable
    protected Date dateFinish;

    @NotNull
    protected String userId;

    @NotNull
    protected Date dateCreate = new Date();

}
