package com.ushakova.tm.command;

import com.ushakova.tm.dto.Domain;
import com.ushakova.tm.exception.empty.EmptyDomainException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getAuthService().logout();
    }

}
