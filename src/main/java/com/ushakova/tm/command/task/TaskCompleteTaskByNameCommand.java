package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskCompleteTaskByNameCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set \"Complete\" status to task by name.";
    }

    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Set Status \"Complete\" to Task***\nEnter Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().completeByName(name, userId);
    }

    @Override
    @NotNull
    public String name() {
        return "complete-task-by-name";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
